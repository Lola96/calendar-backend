package com.example.calenadar.services;

import com.example.calenadar.daos.EventRepository;
import com.example.calenadar.daos.GroupRepository;
import com.example.calenadar.daos.UserRepository;
import com.example.calenadar.entities.Event;
import com.example.calenadar.entities.UserGroup;
import com.example.calenadar.entities.User;
import com.example.calenadar.models.LoginForm;
import com.example.calenadar.models.Response;
import com.example.calenadar.models.UserPrinciple;
import com.example.calenadar.security.JwtProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private UserRepository userRepository;
    private JwtProvider jwtProvider;
    private AuthenticationManager authenticationManager;
    private PasswordEncoder encoder;
    private EventRepository eventRepository;
    private GroupRepository groupRepository;
    private static Logger logger = Logger.getLogger(UserDetailsServiceImpl.class.getName());

    @Autowired
    public UserDetailsServiceImpl(UserRepository userRepository, JwtProvider jwtProvider, AuthenticationManager authenticationManager,
                                  PasswordEncoder encoder, EventRepository eventRepository, GroupRepository groupRepository) {
        this.userRepository = userRepository;
        this.jwtProvider = jwtProvider;
        this.authenticationManager = authenticationManager;
        this.encoder = encoder;
        this.eventRepository = eventRepository;
        this.groupRepository = groupRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email)
            throws UsernameNotFoundException {

        User user = userRepository.findByEmail(email)
                .orElseThrow(() ->
                        new UsernameNotFoundException("user not found with -> username or email : " + email)
                );

        return UserPrinciple.build(user);
    }

    public Response authenticateUser(LoginForm loginForm) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginForm.getEmail(),
                        loginForm.getPassword()
                )
        );

        SecurityContextHolder.getContext().setAuthentication(authentication);

        Optional<User> username = userRepository.findByEmail(loginForm.getEmail());

        if (username.isPresent()) {
            logger.info("authorizeded user " + username.get().getEmail());
        } else throw new RuntimeException("unauthorized user");
        User user = username.get();

        String jwt = jwtProvider.generateJwtToken(authentication);
        return new Response(jwt, user.getId());
    }

    public boolean registerUser(User user) {
        if (userRepository.existsByEmail(user.getEmail())) {
            logger.info(user.getEmail() + " email already in use");
            return false;
        }

        user.setPassword(encoder.encode(user.getPassword()));

        userRepository.save(user);
        logger.info(user.getEmail() + " user registered");
        return true;
    }

    public User getUserById(Long id) {
        return userRepository.findById(id).get();
    }


    public void addEventForStudnet(Long studentId, Event event) {
        eventRepository.save(event);
        Optional<User> user = userRepository.findById(studentId);
        Set<Event> events = user.get().getEvents();
        events.add(event);
        user.get().setEvents(events);
        userRepository.save(user.get());
    }

    public void addGroup(Long studentId, UserGroup group) {
        groupRepository.save(group);
        Optional<User> user = userRepository.findById(studentId);
        Set<UserGroup> groups = user.get().getGroups();
        groups.add(group);
        user.get().setGroups(groups);
        userRepository.save(user.get());
    }


    public Set<Event> getUserEvents(Long id) {
        return userRepository.findById(id).get().getEvents();
    }

    public Set<UserGroup> getUserGroups(Long id) {
        return userRepository.findById(id).get().getGroups();
    }

}

