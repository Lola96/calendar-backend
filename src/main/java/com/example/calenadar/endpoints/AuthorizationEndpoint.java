package com.example.calenadar.endpoints;

import com.example.calenadar.entities.Event;
import com.example.calenadar.entities.UserGroup;
import com.example.calenadar.entities.User;
import com.example.calenadar.models.LoginForm;
import com.example.calenadar.models.Response;
import com.example.calenadar.services.UserDetailsServiceImpl;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class AuthorizationEndpoint {

    private UserDetailsServiceImpl userDetailsService;

    @Autowired
    public AuthorizationEndpoint( UserDetailsServiceImpl userDetailsService) {
        this.userDetailsService = userDetailsService;
    }


    @PostMapping("/signin")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginForm loginForm) {

        Response response = userDetailsService.authenticateUser(loginForm);
        return ResponseEntity.ok(response);
    }

    @PostMapping("/signup")
    public ResponseEntity<String> registerUser(@Valid @RequestBody User user) throws JSONException {

        if (!userDetailsService.registerUser(user))
            return new ResponseEntity<>("Fail -> Email is already in use!", HttpStatus.BAD_REQUEST);

        return ResponseEntity.status(200).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity getUserByUserId(@PathVariable Long id){
        return ResponseEntity.ok(userDetailsService.getUserById(id));
    }

    @PostMapping("/userEvent/{id}")
    public ResponseEntity addEventForStudent(@PathVariable Long id, @RequestBody Event event){
        userDetailsService.addEventForStudnet(id,event);
        return ResponseEntity.status(200).build();
    }

    @PostMapping("/userGroup/{id}")
    public ResponseEntity addGroup(@PathVariable Long id, @RequestBody UserGroup group){
        userDetailsService.addGroup(id,group);
        return ResponseEntity.status(200).build();
    }

    @GetMapping("/studentsEvents/{id}")
    public ResponseEntity getStudentsEntity(@PathVariable Long id){
        return ResponseEntity.ok(userDetailsService.getUserEvents(id));
    }

    @GetMapping("/getGroups/{id}")
    public ResponseEntity getGroups(@PathVariable Long id){
        return ResponseEntity.ok(userDetailsService.getUserGroups(id));
    }

}
