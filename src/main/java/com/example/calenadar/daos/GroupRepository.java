package com.example.calenadar.daos;

import com.example.calenadar.entities.UserGroup;
import org.springframework.data.repository.CrudRepository;

public interface GroupRepository extends CrudRepository<UserGroup, Long> {
}
