package com.example.calenadar.models;

import lombok.Data;

@Data
public class Response {

    private String token;
    private Long userId;

    public Response(String token, Long userId) {
        this.token = token;
        this.userId = userId;
    }
}
