package com.example.calenadar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication public class CalenadarApplication {

    public static void main(String[] args) {
        SpringApplication.run(CalenadarApplication.class, args);
    }

}
