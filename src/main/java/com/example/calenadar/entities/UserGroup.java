package com.example.calenadar.entities;

import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Data
@Entity
@Table
public class UserGroup {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "GROUP_NAME")
    private String groupName;

//
//    @OneToMany(cascade = CascadeType.ALL)
//    @JoinColumn(name = "USER_ID")
//    Set<User> users;

    public UserGroup() {
    }

    public UserGroup(String groupName) {
        this.groupName = groupName;
       // this.users = users;
    }
}
