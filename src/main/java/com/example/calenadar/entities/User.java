package com.example.calenadar.entities;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import java.io.Serializable;
import java.util.Set;

@Data
@Entity
@Table
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "EMAIL")
    @Email
    private String email;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "DISPLAY_NAME")
    private String displayName;

    @Column(name = "DEPARTMENT")
    private String department;

    @Column(name = "COLLAGE")
    private String collage;

    @Column(name = "COURSE")
    private String course;

    @Column(name = "SEMESTER")
    private String semester;

    @Column(name = "PHOTO")
    private String photo;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_ID")
    Set<Event> events;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_ID")
    Set<UserGroup> groups;

    public User() {
    }

    public User(@Email String email, String password) {
        this.email = email;
        this.password = password;
    }

}
